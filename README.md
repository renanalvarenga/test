# Test

## What

> _What is the structure?_

This is a very basic **Java** project that contains all dependencies
to get yourself ready to just code for Muxi test project. You can
find more details of libraries, structure and versions used in this project below.

## Get Started

## Prerequisites

 ```bash

 - Lombok on IDE
 - Database Management and create database named "db-muxi"

 ```

## Steps

 ```bash

 $ git clone https://gitlab.com/renanalvarenga/test.git
 $ cd test

 Create database named "db-muxi" on Database Management
 Run "TestApplication" on IDE

 ```


