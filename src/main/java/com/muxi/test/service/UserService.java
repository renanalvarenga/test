package com.muxi.test.service;

import com.muxi.test.model.User;
import com.muxi.test.repository.UserRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {

    private UserRepository repository;

    UserService(UserRepository userRepository) {
        this.repository = userRepository;
    }

    public List<User> listAll() {
        return repository.findAll();
    }

}
