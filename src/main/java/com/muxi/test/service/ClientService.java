package com.muxi.test.service;

import com.muxi.test.model.Client;
import com.muxi.test.repository.ClientRepository;
import com.muxi.test.repository.UserRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ClientService {

    private ClientRepository repository;
    private UserRepository userRepository;

    ClientService(ClientRepository clientRepository, UserRepository userRepository) {
        this.repository = clientRepository;
        this.userRepository = userRepository;
    }

    public List<Client> listAll() {
        return repository.findAll();
    }

    public Client create(Client client) {
        client.setProposalStatus("pending");
        return repository.save(client);
    }

    public ResponseEntity update(Integer id, String status, Integer userId) {
        Boolean isAnalist = userRepository.findById(userId).get().getId().equals(2);
        if (isAnalist) {
            return repository.findById(id)
                    .map(client -> {
                        client.setProposalStatus(status);
                        Client updated = repository.save(client);
                        return ResponseEntity.ok().body(updated);
                    }).orElse(ResponseEntity.notFound().build());
        } else {
           return ResponseEntity.notFound().build();
        }
    }
}
