package com.muxi.test.controller;

import com.muxi.test.model.Client;
import com.muxi.test.service.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/clients")
public class ClientController {

    private ClientService service;

    ClientController(ClientService clientService) {
        this.service = clientService;
    }

    @GetMapping("")
    public List<Client> list() {
        return service.listAll();
    }

    @PostMapping("")
    public Client create(@RequestBody Client client) {
        return service.create(client);
    }

    @PutMapping(value="/{id}")
    public ResponseEntity update(
            @PathVariable("id") Integer id,
            @RequestParam String status,
            @RequestParam Integer userId
    ) {
        return service.update(id, status, userId);
    }

}
