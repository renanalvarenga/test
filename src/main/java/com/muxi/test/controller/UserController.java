package com.muxi.test.controller;

import com.muxi.test.model.User;
import com.muxi.test.service.UserService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/users")
public class UserController {

    private UserService service;

    UserController(UserService userService) {
        this.service = userService;
    }

    @GetMapping("")
    public List<User> list() {
        return service.listAll();
    }
}
