--liquibase formatted sql

--changeset renan:3
CREATE TABLE CLIENTS(
	id int NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	phone VARCHAR(15) NOT NULL,
	proposal_status VARCHAR(10) NOT NULL,
	proposal_value DECIMAL(60, 2) NOT NULL,
	PRIMARY KEY (id)
);
--rollback DROP TABLE CLIENTS

--changeset renan:4
insert into clients (id, name, email, phone, proposal_status, proposal_value) values (1, 'Renan Alvarenga', 'renan@gmail.com', '1298765432', 'approved', 2500);
insert into clients (id, name, email, phone, proposal_status, proposal_value) values (2, 'Ruy Caetano', 'ruy@gmail.com', '12912345678', 'pending', 4000);
insert into clients (id, name, email, phone, proposal_status, proposal_value) values (3, 'Rosely Silva', 'rosely@gmail.com', '12918273645', 'denied', 1500);
