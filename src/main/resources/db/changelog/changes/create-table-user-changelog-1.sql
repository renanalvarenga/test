--liquibase formatted sql

--changeset renan:1
CREATE TABLE USERS(
	id int NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);
--rollback DROP TABLE USERS

--changeset renan:2
insert into users (id, name) values (1, 'Captação de Propostas');
insert into users (id, name) values (2, 'Analista de Crédito');


