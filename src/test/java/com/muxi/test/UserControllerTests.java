package com.muxi.test;

import com.muxi.test.controller.UserController;
import com.muxi.test.model.User;
import com.muxi.test.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = UserController.class)
public class UserControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private List<User> list;

    @BeforeEach
    void setUp() {
        this.list = new ArrayList<>();
        this.list.add(new User(1, "User 1"));
        this.list.add(new User(2, "User 2"));
    }

    @Test
    void shouldReturnAllUsers() throws Exception {
        given(userService.listAll()).willReturn(this.list);
        this.mockMvc.perform(get("/users"))
                .andExpect(status().isOk());
    }
}
