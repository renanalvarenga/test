package com.muxi.test;

import com.muxi.test.controller.ClientController;
import com.muxi.test.model.Client;
import com.muxi.test.service.ClientService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = ClientController.class)
public class ClientControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClientService clientService;

    private List<Client> list;

    @BeforeEach
    void setUp() {
        this.list = new ArrayList<>();
        this.list.add(new Client(1, "Client 1", "email@email.com", "11111", "pending", 111F));
        this.list.add(new Client(2, "Client 2", "email@email.com", "11111", "pending", 111F));
    }

    @Test
    void shouldReturnAllClients() throws Exception {
        given(clientService.listAll()).willReturn(this.list);
        this.mockMvc.perform(get("/clients"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldCreateClient() throws Exception {
        Client client = new Client(1, "Client 1", "email@email.com", "11111", "pending", 111F);
        given(clientService.create(client)).willReturn(client);
        this.mockMvc.perform(post("/clients")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"Client 1\",\"email\":\"email@email.com\",\"phone\":\"11111\",\"proposalStatus\":\"pending\",\"proposalValue\":\"111\"}")
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldUpdateClient() throws Exception {
        Client client = new Client(1, "Client 1", "email@email.com", "11111", "approved", 111F);
        given(clientService.update(1, "approved", 2)).willReturn(ResponseEntity.ok().body(client));
        this.mockMvc.perform(put("/clients/1?status=approved&userId=2"))
                .andExpect(status().isOk());
    }

    @Test
    void shouldNotUpdateClient() throws Exception {
        Client client = new Client(1, "Client 1", "email@email.com", "11111", "approved", 111F);
        given(clientService.update(1, "approved", 1)).willReturn(ResponseEntity.notFound().build());
        this.mockMvc.perform(put("/clients/1?status=approved&userId=1"))
                .andExpect(status().isNotFound());
    }
}